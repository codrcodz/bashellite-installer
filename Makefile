VERSION := 0.0.1
PATHDIR := /usr/local
BINDIR := $(PATHDIR)/bin
LIBDIR := $(PATHDIR)/lib
CONFDIR := /etc/bashellite
OPTDIR := /opt/bashellite
PROVIDERDIR := $(OPTDIR)/providers.d
LOGDIR := /var/log/bashellite
DOCDIR := /usr/share/doc/bashellite
DATADIR := /var/www/html/bashellite
HOMEDIR := /home/bashellite
CONFIGSGITDIR := ../bashellite-configs
PROVIDERSGITDIR := ../bashellite-providers
BASHELLITEGITDIR := ../bashellite

all: clone useradd install docs

clone:
	# Check for the existence of bashellite repos needed for the install
	# If they exist, do a git update
	# Otherwise do a git clone

	# Bashellite repo
	if [ ! -d $(BASHELLITEGITDIR) ]; then \
		echo "Bashellite repo appears to not exist, attempting to clone repo..."; \
		git clone https://gitlab.com/dreamer-labs/bashellite/bashellite $(BASHELLITEGITDIR); \
	else \
		echo "Bashellite repo appears to exist, attempting to update the repo..."; \
		cd $(BASHELLITEGITDIR); \
		git pull origin master; \
	fi

	# Bashellite configs repo
	if [ ! -d $(CONFIGSGITDIR) ]; then \
		echo "Bashellite-configs repo appears to not exist, attempting to clone repo..."; \
		git clone https://gitlab.com/dreamer-labs/bashellite/bashellite-configs $(CONFIGSGITDIR); \
	else \
		echo "Bashellite-configs repo appears to exist, attempting to update the repo..."; \
		cd $(CONFIGSGITDIR); \
		git pull origin master; \
	fi

	# Bashellite providers repo
	if [ ! -d $(PROVIDERSGITDIR) ]; then \
		echo "Bashellite-providers repo appears to not exist, attempting to clone repo..."; \
		git clone https://gitlab.com/dreamer-labs/bashellite/bashellite-providers $(PROVIDERSGITDIR); \
	else \
		echo "Bashellite-providers repo appears to exist, attempting to update the repo..."; \
		cd $(PROVIDERSGITDIR); \
		git pull origin master; \
	fi


useradd: clone
	# Ensures bashellite user exists
	getent passwd bashellite || useradd -c "Bashellite User" bashellite;
	install -o bashellite -g bashellite -m 0644 -D $(BASHELLITEGITDIR)/home/bashellite/.bashrc $(DESTDIR)$(HOMEDIR)/.bashrc
	install -o bashellite -g bashellite -m 0700 -d $(DESTDIR)$(HOMEDIR)/.bashellite

install: export providers_tld=$(PROVIDERDIR)
install: export bin_dir=$(BINDIR)
install: clone

	# Ensures bashellite program is latest version and ownership & mode is appropriate
	mkdir -p $(DESTDIR)$(BINDIR)
	chown root:root $(DESTDIR)$(BINDIR)
	chmod 2755 $(DESTDIR)$(BINDIR)
	install -o root -g bashellite -m 0550 -D $(BASHELLITEGITDIR)/usr/local/bin/bashellite.sh $(DESTDIR)$(BINDIR)/bashellite

	# Ensures bashellite-libs and util-libs are latest and ownership & mode are appropriate
	mkdir -p $(DESTDIR)$(LIBDIR)
	chown root:root $(DESTDIR)$(LIBDIR)
	chmod 2755 $(DESTDIR)$(LIBDIR)
	install -o root -g bashellite -m 0440 -D $(BASHELLITEGITDIR)/usr/local/lib/bashellite-libs.sh $(DESTDIR)$(LIBDIR)/bashellite-libs.sh
	install -o root -g root -m 0444 -D $(BASHELLITEGITDIR)/usr/local/lib/util-libs.sh $(DESTDIR)$(LIBDIR)/util-libs.sh

	# Ensures provider directory, provider installer, and providers are latest and ownership & modes are appropriate
	mkdir -p $(DESTDIR)$(PROVIDERDIR)
	chmod 2750 $(DESTDIR)$(OPTDIR)
	chmod 2750 $(DESTDIR)$(PROVIDERDIR)
	cp -fR $(BASHELLITEGITDIR)/opt/bashellite/providers.d/test/ $(DESTDIR)$(PROVIDERDIR)/test/
	cp -fR $(PROVIDERSGITDIR)/providers.d/ $(DESTDIR)$(OPTDIR)/ || true
	chown -R root:bashellite $(DESTDIR)/opt/bashellite/
	install -o root -g bashellite -m 0540 -D $(BASHELLITEGITDIR)/opt/bashellite/install_all_providers.sh $(DESTDIR)$(OPTDIR)/install_all_providers.sh
	$(DESTDIR)$(OPTDIR)/install_all_providers.sh
	chmod -R o-wx $(DESTDIR)$(OPTDIR)
	chmod -R g-w $(DESTDIR)$(OPTDIR)

	# Ensures configuration directories exists and ownership & mode are appropriate
	mkdir -p $(DESTDIR)$(CONFDIR)/repos.conf.d/
	chmod 2750 $(DESTDIR)$(CONFDIR)
	cp -fR $(BASHELLITEGITDIR)/etc/bashellite/repos.conf.d/test/ $(DESTDIR)$(CONFDIR)/repos.conf.d/test/
	cp -fR $(CONFIGSGITDIR)/repos.conf.d/ $(DESTDIR)$(CONFDIR)/ || true
	install -o root -g bashellite -m 0740 -D $(BASHELLITEGITDIR)/etc/bashellite/bashellite.conf $(DESTDIR)$(CONFDIR)/bashellite.conf
	install -o root -g bashellite -m 0540 -D $(CONFIGSGITDIR)/bashellite.conf $(DESTDIR)$(CONFDIR)/bashellite.conf || true
	chown -R root:bashellite $(DESTDIR)$(CONFDIR)
	chmod -R o=r $(DESTDIR)$(CONFDIR)
	chmod -R g-x $(DESTDIR)$(CONFDIR)
	chmod -R g=rX $(DESTDIR)$(CONFDIR)
	chmod -R u-x $(DESTDIR)$(CONFDIR)
	chmod -R u=rwX $(DESTDIR)$(CONFDIR) 

	# Ensures log directory exists and ownership & mode are appropriate
	mkdir -p $(DESTDIR)$(LOGDIR)
	chown -R bashellite:bashellite $(DESTDIR)$(LOGDIR)
	chmod 2750 $(DESTDIR)$(LOGDIR)

	# Ensures default mirror directory is created and ownership & mode are appropriate 
	mkdir -p $(DESTDIR)$(DATADIR)
	chown -R bashellite:bashellite $(DESTDIR)$(DATADIR)
	chmod 2750 $(DESTDIR)$(DATADIR)


docs: clone
	# Ensures docs directory and docs exists and mode is set appropriately
	mkdir -p $(DESTDIR)$(DOCDIR)
	install -m 644 -D $(BASHELLITEGITDIR)/README.md $(BASHELLITEGITDIR)/LICENSE $(DESTDIR)$(DOCDIR)


uninstall:

	# Ensures all bashellite configs, bins, libs, logs, and docs are forceably removed and user is deleted
	rm -rf $(DESTDIR)$(BINDIR)/bashellite
	rm -rf $(DESTDIR)$(LIBDIR)/bashellite-libs.sh
	rm -rf $(DESTDIR)$(CONFDIR)
	rm -rf $(DESTDIR)$(LOGDIR)
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -rf $(DESTDIR)$(OPTDIR)


userdel:

	# Ensures bashellite user is removed from system
	userdel -r bashellite


.PHONY: all clone install useradd userdel docs uninstall


